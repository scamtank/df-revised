reaction_revised_empty_bucket

Revision: buckets can fill up with water and sit around uselessly. Let's provide a way to empty them.

[OBJECT:REACTION]

[REACTION:EMPTY_BUCKET]
	[NAME:empty water bucket]
	[DESCRIPTION:Empty a bucket of water, wasting it.]
	[BUILDING:KITCHEN:CUSTOM_E]
	[REAGENT:water:1:LIQUID_MISC:NONE:WATER:NONE]
	[REAGENT:bucket:1:BUCKET:NONE:NONE:NONE]
		[CONTAINS:water]
		[PRESERVE_REAGENT]

