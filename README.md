# Dwarf Fortress as literature.
An editor's pass over Dwarf Fortress: more balanced gameplay, more atmosphere, and fewer bugs. Every creature in the game has a rewritten description and many have simpler names. I want to make the game more pleasant to read and play, without straying too far from the original.

Contributions and criticism are welcome. Help me build a “cathedral”. [1]

Install into a v0.44.09 installation. If you have a saved world, delete it.

## FEATURES
* more detailed creature descriptions (Taffer, Toothspit)
* more specific child and caste names for many creatures. (Modest, Appearance Tweaks, Taffer)
* creatures with unnecessarily long names have had their name shortened. (Taffer)
* forgotten beasts made of stone, wood, and metal can be butchered. (Modest)
* humanoids have pointy or floppy ears where appropriate. (Appearance Tweaks, Taffer)
* arrows and bolts can be carved out of bone in adventure mode. (Wanderer)
* helpful tooltips explain many reactions. (Modest, Meph, Revised)
* feather tree eggs can be gathered and eaten. (Modest)
* humans, elves, and goblins style their hair. (Modest)
* fairy and pixie wings are lacy. (Appearance Tweaks)
* oat beer can be brewed, not just dreamed of. (Modest)

## COMBAT IMPROVEMENTS
* elves wield stronger—and more sensible—wooden weapons. (Coherent Weapons)
* small humanoids can't easily one-hand long swords, which now slash a wider area and can be partly held by the blade. (Coherent Weapons)
* several large spiders and spider people that couldn't trap with their web now can. (Modest)
* creatures without claws or talons can't scratch. (Grimlocke, Taffer)
* predators and violent humanoids won't easily run away. (Wanderer)
* scimitars are larger and slash a wider area. (Coherent Weapons)
* crossbows bolts fire quickly and penetrate better, while arrows are sharper. (Wanderer)
* bone doesn't feel pain, and fat and muscle feel less pain. (Wanderer, Modest)
* whips have been weakened and cost more to create. (Coherent Weapons)
* short swords and spears strike a smaller area. (Coherent Weapons)
* some insects have more accurate and more varied colors. (Warlord255)
* giant predators eat dwarves, and many ambush prey. (Modest)
* body parts have simpler names. (Modest)
* skulls are sturdier and skin is thicker. (Wanderer, Grimlocke)
* many tissues now heal naturally. (Modest)
* short swords penetrate more. (Coherent Weapons)
* many giant creatures can bite. (Modest)
* most humanoids won't bite. (Revised)

## FIXES
* creatures that were required to kill neutral characters now simply like to. (Anonymous, Taffer)
* goblins now age, keeping site populations low for performance reasons. (Anonymous, Taffer)
* many giant creatures previously aged too quickly to be usefully tamed. (Anonymous, Taffer)
* fruit seeds are edible to keep them from stockpiling, for better performance. (Doom Onion)
* animal people and giant animals that spawn in pools spawn in lakes instead. (Modest)
* amphibious mounts have been removed: they kill anybody riding them. (Modest)
* mussels and oysters provide pearls. They no longer yield leather. (Modest)
* color schemes with a blue-tinted green won't have blue vomit. (Taffer)
* fixed some creature products from being listed several times. (Modest)
* tameable animals without CHILD tags can now be tamed. (Modest)
* bark scorpions and their variations now have mouths. (Modest)
* several creatures that should have horns now do. (Modest)
* animal people don't need to eat specific food. (Modest)
* some plant seeds were renamed for flavour. (Modest)
* animal people with legs can always jump. (Modest)
* many flying vermin hunters dive hunt. (Modest)
* toads, lice, and frogs are carnivores. (Modest)
* chitin and shells are used for crafts. (Modest)
* guineafowl have biomes. (Modest)

## OPTIONAL EXTRAS
* dwarves can have “woolly arms, legs, hands, and feet”. Extra hairy! (Taffer)

[1]: http://wryemusings.com/Cathedral%20vs.%20Parlor.html
